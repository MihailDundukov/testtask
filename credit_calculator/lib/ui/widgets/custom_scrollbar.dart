import 'package:flutter/material.dart';

class CustomScrollbar extends StatelessWidget {
  final Widget child;
  final bool alwaysVisibleScrollbar;
  final ScrollController scrollController;
  const CustomScrollbar({
    required this.child,
    required this.scrollController,
    this.alwaysVisibleScrollbar = false,
    Key? key,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) => RawScrollbar(
        padding: EdgeInsets.zero,
        thickness: 5,
        controller: scrollController,
        thumbVisibility: alwaysVisibleScrollbar,
        radius: const Radius.circular(2),
        thumbColor: Colors.grey,
        timeToFade: const Duration(milliseconds: 1000),
        fadeDuration: const Duration(milliseconds: 300),
        child: child,
      );
}
