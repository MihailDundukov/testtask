import 'package:credit_calculator/values/colors.dart';
import 'package:flutter/material.dart';

class DropdownCard extends StatelessWidget {
  final String text;
  const DropdownCard({required this.text, Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) => Container(
        padding: const EdgeInsets.symmetric(
          horizontal: 16,
          vertical: 13,
        ),
        child: Text(
          text,
          style: Theme.of(context).textTheme.titleMedium!.copyWith(
                color: AppColors.hint,
              ),
        ),
      );
}
