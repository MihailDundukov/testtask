import 'dart:async';

import 'package:credit_calculator/ui/canvas/custom_rounded_rectangle_border.dart';
import 'package:credit_calculator/ui/widgets/custom_scrollbar.dart';
import 'package:flutter/material.dart';

class CustomDropDownButton extends StatefulWidget {
  final void Function(int index) onDropdownItemTap;
  final double height;
  final double maxHeight;
  final List<Widget> dropdownWidgets;
  final AlignmentGeometry? alignment;
  final EdgeInsetsGeometry? padding;
  final BoxDecoration? decoration;
  final BoxDecoration? dropdownDecoration;
  final double? width;
  final EdgeInsetsGeometry? margin;
  final Widget value;
  final Color iconColor;
  final bool alwaysVisibleScrollbar;

  const CustomDropDownButton({
    required this.onDropdownItemTap,
    required this.height,
    required this.value,
    required this.dropdownWidgets,
    Key? key,
    this.alignment,
    this.padding,
    this.decoration,
    this.dropdownDecoration,
    this.width,
    this.margin,
    this.maxHeight = 200,
    this.alwaysVisibleScrollbar = false,
    this.iconColor = const Color.fromRGBO(152, 169, 179, 1),
  }) : super(key: key);

  @override
  State<CustomDropDownButton> createState() => _CustomDropDownButtonState();
}

class _CustomDropDownButtonState extends State<CustomDropDownButton>
    with TickerProviderStateMixin {
  late OverlayEntry? _overlayEntry;
  final LayerLink _layerLink = LayerLink();
  bool isActive = false;
  late OverlayState? _overlayState;
  late StreamController<bool> _isOpenedOverlay;
  final ScrollController _scrollController = ScrollController();

  @override
  void initState() {
    _overlayState = Overlay.of(context);
    _isOpenedOverlay = StreamController.broadcast();

    super.initState();
  }

  @override
  void dispose() {
    _isOpenedOverlay.close();
    super.dispose();
  }

  OverlayEntry _createOverlay() {
    final renderBox = context.findRenderObject() as RenderBox?;
    final size = renderBox!.size;
    var height = size.height * widget.dropdownWidgets.length;
    final key = GlobalKey();

    WidgetsBinding.instance.addPostFrameCallback(
      (timeStamp) => setState(() {
        height = key.currentContext!.size!.height;
      }),
    );

    return OverlayEntry(
      builder: (context) => Positioned(
        width: widget.margin == null
            ? size.width
            : size.width - widget.margin!.horizontal,
        child: CompositedTransformFollower(
          link: _layerLink,
          showWhenUnlinked: false,
          offset: Offset(
            0,
            widget.height,
          ),
          child: StreamBuilder<bool>(
            initialData: false,
            stream: _isOpenedOverlay.stream,
            builder: (context, snapshot) => SizedBox(
              width: size.width,
              height: height,
              child: Stack(
                children: [
                  AnimatedPositioned(
                    top: snapshot.data! ? 0 : -height,
                    duration: const Duration(milliseconds: 300),
                    width: widget.margin == null
                        ? size.width
                        : size.width - widget.margin!.horizontal,
                    child: Material(
                      elevation: 0,
                      color: Colors.transparent,
                      child: Container(
                        key: key,
                        constraints: BoxConstraints(
                          maxHeight: widget.maxHeight,
                        ),
                        decoration: ShapeDecoration(
                          color: widget.dropdownDecoration!.color,
                          gradient: widget.dropdownDecoration!.gradient,
                          shape: CustomRoundedRectangleBorder(
                            borderRadius: !isActive
                                ? widget.dropdownDecoration!.borderRadius!
                                    as BorderRadius
                                : widget.dropdownDecoration!.borderRadius ==
                                        null
                                    ? BorderRadius.circular(0)
                                    : BorderRadius.only(
                                        bottomLeft: (widget.dropdownDecoration!
                                                .borderRadius! as BorderRadius)
                                            .bottomLeft,
                                        bottomRight: (widget.dropdownDecoration!
                                                .borderRadius! as BorderRadius)
                                            .bottomRight,
                                      ),
                            bottomSide:
                                (widget.dropdownDecoration!.border! as Border)
                                    .bottom,
                            bottomLeftCornerSide:
                                (widget.dropdownDecoration!.border! as Border)
                                    .bottom,
                            bottomRightCornerSide:
                                (widget.dropdownDecoration!.border! as Border)
                                    .bottom,
                            leftSide:
                                (widget.decoration!.border! as Border).left,
                            rightSide:
                                (widget.decoration!.border! as Border).right,
                          ),
                        ),
                        child: CustomScrollbar(
                          alwaysVisibleScrollbar: widget.alwaysVisibleScrollbar,
                          scrollController: _scrollController,
                          child: ListView.builder(
                            controller: _scrollController,
                            padding: EdgeInsets.zero,
                            itemCount: widget.dropdownWidgets.length,
                            shrinkWrap: true,
                            itemBuilder: (context, index) => GestureDetector(
                              onTap: () {
                                widget.onDropdownItemTap(index);
                                _isOpenedOverlay.add(false);
                                setState(() {
                                  isActive = false;
                                });
                              },
                              child: widget.dropdownWidgets[index],
                            ),
                          ),
                        ),
                      ),
                    ),
                  ),
                ],
              ),
            ),
          ),
        ),
      ),
    );
  }

  @override
  Widget build(BuildContext context) => CompositedTransformTarget(
        link: _layerLink,
        child: GestureDetector(
          onTap: () {
            setState(() {
              isActive = !isActive;
            });
            if (isActive) {
              _overlayEntry = _createOverlay();
              _overlayState!.insert(_overlayEntry!);

              Future.delayed(const Duration(milliseconds: 50), () {
                _isOpenedOverlay.add(true);
              });
            } else {
              _isOpenedOverlay.add(false);
              Future.delayed(
                const Duration(milliseconds: 300),
                () => _overlayEntry!.remove(),
              );
            }
          },
          child: Container(
            height: widget.height,
            width: widget.width,
            decoration: ShapeDecoration(
              color: widget.dropdownDecoration!.color,
              gradient: widget.dropdownDecoration!.gradient,
              shape: CustomRoundedRectangleBorder(
                borderRadius: !isActive
                    ? widget.decoration!.borderRadius! as BorderRadius
                    : widget.decoration!.borderRadius == null
                        ? BorderRadius.circular(0)
                        : BorderRadius.only(
                            topLeft: (widget.decoration!.borderRadius!
                                    as BorderRadius)
                                .topLeft,
                            topRight: (widget.decoration!.borderRadius!
                                    as BorderRadius)
                                .topRight,
                          ),
                bottomSide: !isActive
                    ? (widget.decoration!.border! as Border).bottom
                    : const BorderSide(width: 0),
                bottomLeftCornerSide: !isActive
                    ? (widget.decoration!.border! as Border).bottom
                    : const BorderSide(width: 0),
                bottomRightCornerSide: !isActive
                    ? (widget.decoration!.border! as Border).bottom
                    : const BorderSide(width: 0),
                topLeftCornerSide: (widget.decoration!.border! as Border).top,
                topRightCornerSide: (widget.decoration!.border! as Border).top,
                topSide: (widget.decoration!.border! as Border).top,
                leftSide: (widget.decoration!.border! as Border).left,
                rightSide: (widget.decoration!.border! as Border).right,
              ),
            ),
            child: Container(
              height: widget.height,
              width: widget.width,
              alignment: widget.alignment,
              padding: widget.padding,
              margin: widget.margin,
              decoration: ShapeDecoration(
                color: widget.decoration!.color,
                gradient: widget.decoration!.gradient,
                shape: CustomRoundedRectangleBorder(
                  borderRadius: !isActive
                      ? widget.decoration!.borderRadius! as BorderRadius
                      : widget.decoration!.borderRadius == null
                          ? BorderRadius.circular(0)
                          : widget.decoration!.borderRadius! as BorderRadius,
                  bottomSide: isActive
                      ? (widget.decoration!.border! as Border).bottom
                      : const BorderSide(width: 0),
                  bottomLeftCornerSide: isActive
                      ? (widget.decoration!.border! as Border).bottom
                      : const BorderSide(width: 0),
                  bottomRightCornerSide: isActive
                      ? (widget.decoration!.border! as Border).bottom
                      : const BorderSide(width: 0),
                  topSide: const BorderSide(width: 0),
                  leftSide: const BorderSide(width: 0),
                  rightSide: const BorderSide(width: 0),
                ),
              ),
              child: Row(
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                children: [
                  widget.value,
                  Icon(
                    !isActive
                        ? Icons.keyboard_arrow_down_rounded
                        : Icons.keyboard_arrow_up_rounded,
                    size: 32,
                    color: widget.iconColor,
                  ),
                ],
              ),
            ),
          ),
        ),
      );
}
