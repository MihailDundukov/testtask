import 'package:credit_calculator/data/models/payment_model.dart';

abstract class CalculateRepository {
  Future<int> savePayment(PaymentModel model);
  Future<List<PaymentModel>> getAllPayments();
}