import 'package:credit_calculator/di/bindings/bindings.dart';
import 'package:credit_calculator/screens/calculator_screen/calculator_screen.dart';
import 'package:credit_calculator/values/text.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';

void main() {
  runApp(const MyApp());
}

class MyApp extends StatelessWidget {
  const MyApp({super.key});

  @override
  Widget build(BuildContext context) {
    return GetMaterialApp(
      theme: ThemeData(
        appBarTheme: const AppBarTheme(
          toolbarHeight: 56,
        ),
        textTheme: AppTextTheme.textTheme,
      ),
      initialBinding: CalculatorBindings(),
      home: const CalculatorScreen(),
    );
  }
}
