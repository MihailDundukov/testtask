class AppStrings {
  //errors
  static const error = 'Ошибка';

  static const errorToAdd = 'Возникла ошибка при сохранении записи';

  static const errorToGet = 'Возникла ошибка при получении истории запросов';

  static const emptyHistory = 'Ваша история запросов пуста';

  static const inputOnlyCorrect = 'Вводите корректные данные';

  //units
  static const rub = 'руб.';

  static const yearly = 'годовых';

  //ui
  static const appName = 'Калькулятор кредита';

  static const requestDetalisation = 'Детализация запроса';

  static const history = 'История запросов';

  static const summ = 'Сумма кредита';

  static const percents = 'Проценты';

  static const year = 'Год';

  static const mounth = 'Месяц';

  static const calculate = 'Посчитать';

  static const annuitent = 'Аннуитентный платеж';

  static const defferential = 'Дифференцированный платеж';

  static const firstMounthPayment = 'Выплата в первый месяц';

  static const lastMounthPaymnet = 'Выплата в последний месяц';

  static const overpayments = 'Переплата';

  static const paymnetGraphic = 'График выплат:';
}