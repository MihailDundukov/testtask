import 'package:flutter/material.dart';

class AppIcons {
  static const delete = Icons.close;

  static const info = Icons.info_outline;

  static const history = Icons.history;
}