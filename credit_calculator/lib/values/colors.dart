import 'package:flutter/material.dart';

class AppColors {
  static const scaffoldBg = Colors.white;

  static const bgFront = Colors.white;

  static const icon = Colors.grey;

  static const icon2 = Colors.white;

  static const border = Color.fromRGBO(117, 117, 117, 1);

  static const action = Color.fromRGBO(255, 167, 38, 1);

  static const text = Colors.black;

  static const hint = Color.fromRGBO(189, 189, 189, 1);

  static const error = Color.fromRGBO(252, 207, 215, 1);

  static const shadow = Colors.black26;
}