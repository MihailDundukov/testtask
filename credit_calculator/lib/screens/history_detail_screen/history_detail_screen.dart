import 'package:credit_calculator/data/models/payment_model.dart';
import 'package:credit_calculator/getx_controllers/calculator_controller.dart';
import 'package:credit_calculator/screens/calculator_screen/calculate_result.dart';
import 'package:credit_calculator/screens/history_detail_screen/insert_values.dart';
import 'package:credit_calculator/values/colors.dart';
import 'package:credit_calculator/values/strings.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';

class HistoryDetailScreen extends StatefulWidget {
  final PaymentModel model;
  const HistoryDetailScreen({required this.model, super.key});

  @override
  State<HistoryDetailScreen> createState() => _HistoryDetailScreenState();
}

class _HistoryDetailScreenState extends State<HistoryDetailScreen> {
  final _controller = Get.find<CalculatorController>();

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: AppColors.scaffoldBg,
      appBar: AppBar(
        title: Text(
          AppStrings.requestDetalisation,
          style: Theme.of(context)
              .textTheme
              .displayMedium!
              .copyWith(color: AppColors.text),
        ),
      ),
      body: Padding(
        padding: const EdgeInsets.all(16),
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            InsertValues(
              model: widget.model.insertModel,
            ),
            const SizedBox(
              height: 16,
            ),
            CalculateResult(
              model: widget.model,
              data: _controller.chartData(widget.model),
            ),
          ],
        ),
      ),
    );
  }
}
