import 'package:credit_calculator/data/models/insert_model.dart';
import 'package:credit_calculator/utils/enums/date_type.dart';
import 'package:credit_calculator/values/colors.dart';
import 'package:credit_calculator/values/strings.dart';
import 'package:flutter/material.dart';

class InsertValues extends StatelessWidget {
  final InsertModel model;
  const InsertValues({
    required this.model,
    super.key,
  });

  String _getDate() {
    int lastSymb =
        int.parse(model.date.toString()[model.date.toString().length - 1]);
    if (model.dateType == DateType.year) {
      if (lastSymb == 1) {
        return 'год';
      } else if (lastSymb > 1 && lastSymb < 5) {
        return 'года';
      } else {
        return 'лет';
      }
    } else {
      if (lastSymb == 1) {
        return 'месяц';
      } else if (lastSymb > 1 && lastSymb < 5) {
        return 'месяца';
      } else {
        return 'месяцев';
      }
    }
  }

  @override
  Widget build(BuildContext context) {
    return Column(
      crossAxisAlignment: CrossAxisAlignment.start,
      children: [
        Text(
          '${model.summ} ${AppStrings.rub} под ${model.percents}% ${AppStrings.yearly}',
          style: Theme.of(context)
              .textTheme
              .titleMedium!
              .copyWith(color: AppColors.text),
        ),
        const SizedBox(
          height: 8,
        ),
        Text(
          'На ${model.date} ${_getDate()}',
          style: Theme.of(context)
              .textTheme
              .titleMedium!
              .copyWith(color: AppColors.text),
        ),
      ],
    );
  }
}
