import 'package:credit_calculator/data/models/insert_model.dart';
import 'package:credit_calculator/ui/widgets/custom_dropdown_button.dart';
import 'package:credit_calculator/ui/widgets/dropdown_card.dart';
import 'package:credit_calculator/utils/enums/date_type.dart';
import 'package:credit_calculator/utils/enums/payment_type.dart';
import 'package:credit_calculator/utils/formatters/only_int_format.dart';
import 'package:credit_calculator/utils/formatters/only_num_format.dart';
import 'package:credit_calculator/values/colors.dart';
import 'package:credit_calculator/values/icons.dart';
import 'package:credit_calculator/values/strings.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';

class InsertData extends StatefulWidget {
  final void Function(InsertModel) calculateCallback;
  const InsertData({
    required this.calculateCallback,
    super.key,
  });

  @override
  State<InsertData> createState() => _InsertDataState();
}

class _InsertDataState extends State<InsertData> {
  final _summController = TextEditingController();
  final _percentController = TextEditingController();
  final _dateController = TextEditingController();
  final Rx<DateType> _selectedDateType = DateType.mounth.obs;
  final Rx<PaymentType> _selectedPaymentType = PaymentType.annuitent.obs;

  final _dateTypes = [DateType.mounth, DateType.year];
  final _paymentTypes = [PaymentType.annuitent, PaymentType.differential];

  String _getDateTypeName(DateType type) {
    switch (type) {
      case DateType.mounth:
        return AppStrings.mounth;
      case DateType.year:
        return AppStrings.year;
    }
  }

  String _getPaymentTypeName(PaymentType type) {
    switch (type) {
      case PaymentType.annuitent:
        return AppStrings.annuitent;
      case PaymentType.differential:
        return AppStrings.defferential;
    }
  }

  InputDecoration getDecoration(TextEditingController controller) =>
      InputDecoration(
        suffixIcon: GestureDetector(
          behavior: HitTestBehavior.translucent,
          onTap: () {
            controller.clear();
          },
          child: const Padding(
            padding: EdgeInsets.only(left: 6),
            child: Icon(
              AppIcons.delete,
              color: AppColors.icon,
            ),
          ),
        ),
        filled: true,
        counterText: '',
        fillColor: AppColors.bgFront,
        contentPadding: const EdgeInsets.only(
          bottom: 10,
          left: 8,
        ),
        hintStyle: Theme.of(Get.context!).textTheme.titleMedium!.copyWith(
              color: AppColors.hint,
            ),
        border: InputBorder.none,
        errorBorder: InputBorder.none,
        disabledBorder: InputBorder.none,
        enabledBorder: const OutlineInputBorder(
          borderSide: BorderSide(
            width: 1,
            color: AppColors.border,
          ),
        ),
        focusedBorder: const OutlineInputBorder(
          borderSide: BorderSide(
            width: 1,
            color: AppColors.border,
          ),
        ),
        focusedErrorBorder: InputBorder.none,
      );

  @override
  Widget build(BuildContext context) {
    return Column(
      children: [
        SizedBox(
          height: 40,
          child: TextField(
            keyboardType: TextInputType.number,
            controller: _summController,
            inputFormatters: [OnlyIntFormatter()],
            style: Theme.of(context).textTheme.titleMedium!.copyWith(
                  color: AppColors.text,
                ),
            decoration: getDecoration(_summController).copyWith(
              hintText: AppStrings.summ,
            ),
          ),
        ),
        const SizedBox(
          height: 16,
        ),
        Row(
          children: [
            Expanded(
              child: SizedBox(
                height: 40,
                child: TextField(
                  controller: _percentController,
                  keyboardType: TextInputType.number,
                  inputFormatters: [OnlyNumFormatter()],
                  style: Theme.of(context).textTheme.titleMedium!.copyWith(
                        color: AppColors.text,
                      ),
                  decoration: getDecoration(_percentController).copyWith(
                    hintText: AppStrings.percents,
                  ),
                ),
              ),
            ),
            const SizedBox(
              width: 12,
            ),
            Expanded(
              child: SizedBox(
                height: 40,
                child: TextField(
                  controller: _dateController,
                  keyboardType: TextInputType.number,
                  inputFormatters: [OnlyIntFormatter()],
                  style: Theme.of(context).textTheme.titleMedium!.copyWith(
                        color: AppColors.text,
                      ),
                  decoration: getDecoration(_dateController).copyWith(
                    hintText: '',
                  ),
                ),
              ),
            ),
            const SizedBox(
              width: 12,
            ),
            Expanded(
              child: CustomDropDownButton(
                onDropdownItemTap: (index) {
                  _selectedDateType.value = _dateTypes[index];
                },
                height: 40,
                value: Obx(
                  () => Padding(
                    padding: const EdgeInsets.symmetric(
                      horizontal: 16,
                    ),
                    child: Text(
                      _getDateTypeName(_selectedDateType.value),
                      style: Theme.of(context).textTheme.titleMedium!.copyWith(
                            color: AppColors.text,
                          ),
                    ),
                  ),
                ),
                dropdownWidgets: List.generate(
                  _dateTypes.length,
                  (index) => DropdownCard(
                    text: _getDateTypeName(_dateTypes[index]),
                  ),
                ),
                decoration: BoxDecoration(
                  color: AppColors.bgFront,
                  borderRadius: BorderRadius.circular(8),
                  border: Border.all(
                    width: 1,
                    color: AppColors.border,
                  ),
                ),
                dropdownDecoration: const BoxDecoration(
                  color: AppColors.bgFront,
                  borderRadius: BorderRadius.only(
                    bottomLeft: Radius.circular(8),
                    bottomRight: Radius.circular(8),
                  ),
                  border: Border(
                    right: BorderSide(
                      width: 1,
                      color: AppColors.border,
                    ),
                    left: BorderSide(
                      width: 1,
                      color: AppColors.border,
                    ),
                    bottom: BorderSide(
                      width: 1,
                      color: AppColors.border,
                    ),
                  ),
                ),
              ),
            ),
          ],
        ),
        const SizedBox(
          height: 16,
        ),
        CustomDropDownButton(
          onDropdownItemTap: (index) {
            _selectedPaymentType.value = _paymentTypes[index];
          },
          height: 40,
          value: Obx(
            () => Padding(
              padding: const EdgeInsets.symmetric(
                horizontal: 16,
              ),
              child: Text(
                _getPaymentTypeName(_selectedPaymentType.value),
                style: Theme.of(context).textTheme.titleMedium!.copyWith(
                      color: AppColors.text,
                    ),
              ),
            ),
          ),
          dropdownWidgets: List.generate(
            _paymentTypes.length,
            (index) => DropdownCard(
              text: _getPaymentTypeName(_paymentTypes[index]),
            ),
          ),
          decoration: BoxDecoration(
            color: AppColors.bgFront,
            borderRadius: BorderRadius.circular(8),
            border: Border.all(
              width: 1,
              color: AppColors.border,
            ),
          ),
          dropdownDecoration: const BoxDecoration(
            color: AppColors.bgFront,
            borderRadius: BorderRadius.only(
              bottomLeft: Radius.circular(8),
              bottomRight: Radius.circular(8),
            ),
            border: Border(
              right: BorderSide(
                width: 1,
                color: AppColors.border,
              ),
              left: BorderSide(
                width: 1,
                color: AppColors.border,
              ),
              bottom: BorderSide(
                width: 1,
                color: AppColors.border,
              ),
            ),
          ),
        ),
        const SizedBox(
          height: 16,
        ),
        GestureDetector(
          onTap: () {
            FocusManager.instance.primaryFocus?.unfocus();
            widget.calculateCallback(InsertModel.fromRawData(
              summ: _summController.text,
              percents: _percentController.text,
              date: _dateController.text,
              dateType: _selectedDateType.value,
              paymentType: _selectedPaymentType.value,
            ));
          },
          child: Container(
            height: 52,
            decoration: BoxDecoration(
              borderRadius: BorderRadius.circular(8),
              color: AppColors.action,
            ),
            alignment: Alignment.center,
            child: Text(
              AppStrings.calculate,
              style: Theme.of(context).textTheme.headlineMedium!.copyWith(
                    color: AppColors.text,
                  ),
            ),
          ),
        ),
      ],
    );
  }
}
