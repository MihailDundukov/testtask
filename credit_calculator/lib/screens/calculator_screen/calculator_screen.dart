import 'package:credit_calculator/getx_controllers/calculator_controller.dart';
import 'package:credit_calculator/screens/calculator_screen/calculate_result.dart';
import 'package:credit_calculator/screens/calculator_screen/insert_data.dart';
import 'package:credit_calculator/screens/history_sceen/history_screen.dart';
import 'package:credit_calculator/utils/enums/state_type.dart';
import 'package:credit_calculator/values/colors.dart';
import 'package:credit_calculator/values/icons.dart';
import 'package:credit_calculator/values/strings.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';

class CalculatorScreen extends StatefulWidget {
  const CalculatorScreen({super.key});

  @override
  State<CalculatorScreen> createState() => _CalculatorScreenState();
}

class _CalculatorScreenState extends State<CalculatorScreen> {
  final _controller = Get.find<CalculatorController>();

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: AppColors.scaffoldBg,
      appBar: AppBar(
        title: Text(
          AppStrings.appName,
          style: Theme.of(context)
              .textTheme
              .displayMedium!
              .copyWith(color: AppColors.text),
        ),
        actions: [
          GestureDetector(
            behavior: HitTestBehavior.translucent,
            onTap: () {
              FocusManager.instance.primaryFocus?.unfocus();
              Get.to(() => const HistoryScreen());
            },
            child: const Icon(
              AppIcons.history,
              color: AppColors.icon2,
            ),
          ),
          const SizedBox(
            width: 16,
          ),
        ],
      ),
      body: GetBuilder<CalculatorController>(
        init: _controller,
        builder: (_) {
          return Padding(
            padding: const EdgeInsets.all(16),
            child: SingleChildScrollView(
              child: Column(
                mainAxisSize: MainAxisSize.min,
                children: [
                  InsertData(
                    calculateCallback: (model) {
                      _controller.calculate(model);
                    },
                  ),
                  const SizedBox(
                    height: 16,
                  ),
                  if (_controller.state == StateType.loading)
                    const Center(
                      child: CupertinoActivityIndicator(
                        color: AppColors.icon,
                      ),
                    ),
                  if (_controller.state == StateType.done)
                    CalculateResult(
                      model: _controller.paymentModel,
                      data: _controller.chartData(null),
                    ),
                ],
              ),
            ),
          );
        },
      ),
    );
  }
}
