import 'package:credit_calculator/data/models/payment_model.dart';
import 'package:credit_calculator/values/colors.dart';
import 'package:credit_calculator/values/strings.dart';
import 'package:flutter/material.dart';
import 'package:graphic/graphic.dart';

class CalculateResult extends StatelessWidget {
  final PaymentModel? model;
  final List<Map<String, dynamic>> data;
  const CalculateResult({
    required this.model,
    required this.data,
    super.key,
  });

  @override
  Widget build(BuildContext context) {
    return Column(
      crossAxisAlignment: CrossAxisAlignment.start,
      children: [
        Text(
          '${AppStrings.firstMounthPayment}: ${model!.paymentPerFirstMounth}',
          style: Theme.of(context)
              .textTheme
              .titleMedium!
              .copyWith(color: AppColors.text),
        ),
        const SizedBox(
          height: 12,
        ),
        Text(
          '${AppStrings.lastMounthPaymnet}: ${model!.paymentPerLastMounth}',
          style: Theme.of(context)
              .textTheme
              .titleMedium!
              .copyWith(color: AppColors.text),
        ),
        const SizedBox(
          height: 12,
        ),
        Text(
          '${AppStrings.overpayments}: ${model!.overpayments}',
          style: Theme.of(context)
              .textTheme
              .titleMedium!
              .copyWith(color: AppColors.text),
        ),
        const SizedBox(
          height: 16,
        ),
        Text(
          AppStrings.paymnetGraphic,
          style: Theme.of(context)
              .textTheme
              .titleMedium!
              .copyWith(color: AppColors.text),
        ),
        const SizedBox(
          height: 12,
        ),
        SingleChildScrollView(
          scrollDirection: Axis.horizontal,
          child: SizedBox(
            height: 300,
            width: 800,
            child: Chart(
              data: data,
              variables: {
                'Месяц': Variable(
                  accessor: (Map map) => map['mounth'] as int,
                ),
                'Выплата': Variable(
                  accessor: (Map map) => map['payment'] as num,
                ),
              },
              marks: [
                IntervalMark(
                  elevation: ElevationEncode(
                    value: 0,
                    updaters: {
                      'tap': {true: (_) => 5}
                    },
                  ),
                  color: ColorEncode(
                    value: Defaults.primaryColor,
                    updaters: {
                      'tap': {false: (color) => color.withAlpha(100)}
                    },
                  ),
                )
              ],
              axes: [
                Defaults.horizontalAxis,
                Defaults.verticalAxis,
              ],
              selections: {'tap': PointSelection(dim: Dim.x)},
              tooltip: TooltipGuide(),
              crosshair: CrosshairGuide(),
            ),
          ),
        ),
      ],
    );
  }
}
