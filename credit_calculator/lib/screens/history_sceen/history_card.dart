import 'package:credit_calculator/data/models/payment_model.dart';
import 'package:credit_calculator/screens/history_detail_screen/history_detail_screen.dart';
import 'package:credit_calculator/values/colors.dart';
import 'package:credit_calculator/values/strings.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';

class HistroyCard extends StatelessWidget {
  final PaymentModel model;
  const HistroyCard({
    required this.model,
    super.key,
  });

  @override
  Widget build(BuildContext context) {
    return GestureDetector(
      onTap: () {
        Get.to(() => HistoryDetailScreen(model: model));
      },
      child: Container(
        margin: const EdgeInsets.all(16).copyWith(bottom: 0),
        padding: const EdgeInsets.all(12),
        decoration: BoxDecoration(
          borderRadius: BorderRadius.circular(8),
          color: AppColors.bgFront,
          boxShadow: const [
            BoxShadow(
              color: AppColors.shadow,
              offset: Offset(0, -5),
              blurRadius: 2,
            ),
          ],
        ),
        child: Column(
          children: [
            Text(
              '${model.insertModel.summ} ${AppStrings.rub}',
              style: Theme.of(context)
                  .textTheme
                  .titleMedium!
                  .copyWith(color: AppColors.text),
            ),
            const SizedBox(
              height: 16,
            ),
            Text(
              'Под ${model.insertModel.percents}% ${AppStrings.yearly}',
              style: Theme.of(context)
                  .textTheme
                  .titleMedium!
                  .copyWith(color: AppColors.text),
            ),
          ],
        ),
      ),
    );
  }
}
