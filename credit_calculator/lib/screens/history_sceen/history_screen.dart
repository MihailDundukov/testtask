import 'package:credit_calculator/getx_controllers/history_controller.dart';
import 'package:credit_calculator/screens/history_sceen/history_card.dart';
import 'package:credit_calculator/values/colors.dart';
import 'package:credit_calculator/values/strings.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';

class HistoryScreen extends StatefulWidget {
  const HistoryScreen({super.key});

  @override
  State<HistoryScreen> createState() => _HistoryScreenState();
}

class _HistoryScreenState extends State<HistoryScreen> {
  final _controller = Get.find<HistoryController>();

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: AppColors.scaffoldBg,
      appBar: AppBar(
        title: Text(
          AppStrings.history,
          style: Theme.of(context)
              .textTheme
              .displayMedium!
              .copyWith(color: AppColors.text),
        ),
      ),
      body: FutureBuilder(
        future: _controller.getData(),
        builder: (ctx, snapshot) {
          if (snapshot.hasData) {
            return GetBuilder<HistoryController>(
              init: _controller,
              builder: (_) {
                if (_controller.data.isEmpty) {
                  return Center(
                    child: Text(
                      AppStrings.emptyHistory,
                      style: Theme.of(context)
                          .textTheme
                          .titleMedium!
                          .copyWith(color: AppColors.text),
                    ),
                  );
                } else {
                  return RawScrollbar(
                    child: ListView.builder(
                      padding: EdgeInsets.zero,
                      itemCount: _controller.data.length,
                      itemBuilder: (ctx, index) {
                        return HistroyCard(
                          model: _controller.data[index],
                        );
                      },
                    ),
                  );
                }
              },
            );
          } else if (snapshot.hasError) {
            return Center(
              child: Text(
                AppStrings.errorToGet,
                style: Theme.of(context)
                    .textTheme
                    .titleMedium!
                    .copyWith(color: AppColors.text),
              ),
            );
          } else {
            return const Center(
              child: CupertinoActivityIndicator(
                color: AppColors.icon,
              ),
            );
          }
        },
      ),
    );
  }
}
