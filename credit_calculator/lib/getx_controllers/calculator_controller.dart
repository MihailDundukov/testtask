import 'package:credit_calculator/data/models/insert_model.dart';
import 'package:credit_calculator/data/models/payment_model.dart';
import 'package:credit_calculator/data/repositories_impl/calculate_repository_impl.dart';
import 'package:credit_calculator/repositories/calculate_repository.dart';
import 'package:credit_calculator/ui/widgets/custom_error_widget.dart';
import 'package:credit_calculator/utils/enums/payment_type.dart';
import 'package:credit_calculator/utils/enums/state_type.dart';
import 'package:credit_calculator/values/colors.dart';
import 'package:get/get.dart';
import 'dart:math' as math;

class CalculatorController extends GetxController {
  StateType state = StateType.waiting;
  PaymentModel? paymentModel;
  CalculateRepository repository = CalculateRepositoryImpl();

  void calculate(InsertModel model) {
    _setState(StateType.loading);
    if (model.paymentType == PaymentType.annuitent) {
      double paymentPerFirstMounth = _annuitentPaymentsPerMounth(model);
      double fullPayments = paymentPerFirstMounth * model.date;

      paymentModel = PaymentModel(
        paymentPerFirstMounth: paymentPerFirstMounth,
        paymentPerLastMounth: paymentPerFirstMounth,
        fullPayments: fullPayments,
        insertModel: model,
      );
    } else if (model.paymentType == PaymentType.differential) {
      double paymentPerFirstMounth = _differentialPaymentPerMouth(1, model);
      double paymentPerLastMounth =
          _differentialPaymentPerMouth(model.date, model);
      double fullPayments = _fullDefferentialPayment(model);

      paymentModel = PaymentModel(
        paymentPerFirstMounth: paymentPerFirstMounth,
        paymentPerLastMounth: paymentPerLastMounth,
        fullPayments: fullPayments,
        insertModel: model,
      );
    }

    _setState(StateType.done);
    try {
      repository.savePayment(paymentModel!);
    } catch (e) {
      Get.showSnackbar(
        GetSnackBar(
          backgroundColor: AppColors.error,
          messageText: CustomErrorWidget(
            message: e.toString(),
          ),
        ),
      );
    }
  }

  void _setState(StateType newType) {
    if (newType != state) {
      state = newType;
      update();
    }
  }

  List<Map<String, dynamic>> chartData(PaymentModel? model) {
    if ((model ?? paymentModel!).insertModel.paymentType ==
        PaymentType.annuitent) {
      return List.generate(
          (model ?? paymentModel!).insertModel.date,
          (index) => <String, dynamic>{
                'mounth': index + 1,
                'payment': (model ?? paymentModel!).paymentPerFirstMounth,
              });
    } else {
      List<Map<String, dynamic>> data = [];
      for (int i = 0; i < (model ?? paymentModel!).insertModel.date; i++) {
        data.add(<String, dynamic>{
          'mounth': i + 1,
          'payment': _differentialPaymentPerMouth(
              i + 1, (model ?? paymentModel!).insertModel),
        });
      }

      return data;
    }
  }

  double _differentialPaymentPerMouth(int mounth, InsertModel model) {
    double ost = model.summ - (model.summ / model.date) * (mounth - 1);
    double paymentPerMounth =
        (model.summ / model.date) + ost * model.percents / (100 * 12);

    return paymentPerMounth.roundToDouble();
  }

  double _fullDefferentialPayment(InsertModel model) {
    double payment = model.summ +
        model.date * model.summ * model.percents / (100 * 12) +
        1.5 *
            ((model.summ * model.percents / (100 * 12) * (model.date + 1)) /
                model.date) -
        0.5 *
            ((model.summ *
                    model.percents /
                    (100 * 12) *
                    math.pow(model.date + 1, 2)) /
                model.date) -
        ((model.summ * model.percents / (100 * 12)) / model.date);

    return payment.roundToDouble();
  }

  double _annuitentPaymentsPerMounth(InsertModel model) {
    double chisl = model.percents / (100 * 12);
    double znam =
        1.0 - math.pow((1 + model.percents / (100 * 12)), -model.date);
    double annutientKoef = chisl / znam;

    double paymentPerMounth = model.summ * annutientKoef;

    return paymentPerMounth.roundToDouble();
  }
}
