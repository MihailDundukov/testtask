import 'package:credit_calculator/data/models/payment_model.dart';
import 'package:credit_calculator/data/repositories_impl/calculate_repository_impl.dart';
import 'package:credit_calculator/repositories/calculate_repository.dart';
import 'package:credit_calculator/ui/widgets/custom_error_widget.dart';
import 'package:credit_calculator/values/colors.dart';
import 'package:get/get.dart';

class HistoryController extends GetxController {
  List<PaymentModel> data = [];
  CalculateRepository repository = CalculateRepositoryImpl();

  Future<int> getData() async {
    try {
      data = List.from(await repository.getAllPayments());
    } catch (e) {
      Get.showSnackbar(
        GetSnackBar(
          backgroundColor: AppColors.error,
          messageText: CustomErrorWidget(
            message: e.toString(),
          ),
        ),
      );
    }

    return 1;
  }
}
