import 'package:credit_calculator/data/models/payment_model.dart';
import 'package:credit_calculator/repositories/calculate_repository.dart';
import 'package:credit_calculator/utils/db_utils/database_helper.dart';
import 'package:credit_calculator/values/strings.dart';
import 'package:sqflite/sqflite.dart';

class CalculateRepositoryImpl implements CalculateRepository {
  final databaseHelper = DatabaseHelper();

  @override
  Future<List<PaymentModel>> getAllPayments() async {
    try {
      Database db = await databaseHelper.database;
      List<Map<String, dynamic>> mapList =
          await db.query(databaseHelper.tableName, orderBy: '${databaseHelper.colId} DESC');

      List<PaymentModel> result = [];
      for (var map in mapList) {
        result.add(PaymentModel.fromMap(map));
      }
      return result;
    } on Exception {
      throw Exception(AppStrings.errorToGet);
    }
  }

  @override
  Future<int> savePayment(PaymentModel model) async {
    try {
      Database db = await databaseHelper.database;
      int result = await db.insert(databaseHelper.tableName, model.toMap());

      return result;
    } on Exception {
      throw Exception(AppStrings.errorToAdd);
    }
  }

}