import 'package:credit_calculator/utils/enums/date_type.dart';
import 'package:credit_calculator/utils/enums/payment_type.dart';

class InsertModel {
  final int _summ;
  final double _percents;
  final int _date;
  final DateType _dateType;
  final PaymentType _paymentType;

  int get summ => _summ;
  double get percents => _percents;
  int get date => _date;
  DateType get dateType => _dateType;
  PaymentType get paymentType => _paymentType;

  const InsertModel({
    required int summ,
    required double percents,
    required int date,
    required DateType dateType,
    required PaymentType paymentType,
  })  : _summ = summ,
        _percents = percents,
        _date = date,
        _dateType = dateType,
        _paymentType = paymentType;

  factory InsertModel.fromRawData({
    required String summ,
    required String percents,
    required String date,
    required DateType dateType,
    required PaymentType paymentType,
  }) =>
      InsertModel(
        summ: int.parse(summ),
        percents: double.parse(percents),
        date:
            dateType == DateType.year ? 12 * int.parse(date) : int.parse(date),
        dateType: dateType,
        paymentType: paymentType,
      );

  factory InsertModel.fromMap(Map<String, dynamic> map) => InsertModel(
        summ: map['summ'],
        percents: map['percents'],
        date: map['date'],
        dateType: map['date_type'].toString() == 'y'
            ? DateType.year
            : DateType.mounth,
        paymentType: map['payment_type'].toString() == 'ann'
            ? PaymentType.annuitent
            : PaymentType.differential,
      );
}
