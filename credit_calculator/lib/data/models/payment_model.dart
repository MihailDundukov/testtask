import 'package:credit_calculator/data/models/insert_model.dart';
import 'package:credit_calculator/utils/enums/date_type.dart';
import 'package:credit_calculator/utils/enums/payment_type.dart';

class PaymentModel {
  final double paymentPerFirstMounth;
  final double paymentPerLastMounth;
  final double fullPayments;
  final InsertModel insertModel;

  const PaymentModel({
    required this.paymentPerFirstMounth,
    required this.paymentPerLastMounth,
    required this.fullPayments,
    required this.insertModel,
  });

  double get overpayments => fullPayments - insertModel.summ;

  String _getStringDateType() {
    switch (insertModel.dateType) {
      case DateType.year:
        return 'y';
      case DateType.mounth:
        return 'm';
    }
  }

  String _getStringPaymentType() {
    switch (insertModel.paymentType) {
      case PaymentType.annuitent:
        return 'ann';
      case PaymentType.differential:
        return 'dif';
    }
  }

  factory PaymentModel.fromMap(Map<String, dynamic> map) => PaymentModel(
        paymentPerFirstMounth: map['first_mounth'],
        paymentPerLastMounth: map['last_mounth'],
        fullPayments: map['full_payments'],
        insertModel: InsertModel.fromMap(map),
      );

  Map<String, dynamic> toMap() => <String, dynamic>{
        'first_mounth': paymentPerFirstMounth,
        'last_mounth': paymentPerLastMounth,
        'full_payments': fullPayments,
        'summ': insertModel.summ,
        'percents': insertModel.percents,
        'date': insertModel.date,
        'date_type': _getStringDateType(),
        'payment_type': _getStringPaymentType(),
      };
}
