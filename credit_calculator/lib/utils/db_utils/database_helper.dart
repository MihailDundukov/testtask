import 'dart:io';

import 'package:sqflite/sqflite.dart';
import 'package:path_provider/path_provider.dart';

class DatabaseHelper {
  static final _databaseHelper = DatabaseHelper._createInstance();
  Database? _database;

  String tableName = 'paymentss_table';

  String colId = 'id';
  String colFirstMounth = 'first_mounth';
  String colLastMounth = 'last_mounth';
  String colFullPayments = 'full_payments';
  String colSumm = 'summ';
  String colPercents = 'percents';
  String colDate = 'date';
  String colDateType = 'date_type';
  String colPaymentType = 'payment_type';

  DatabaseHelper._createInstance();

  factory DatabaseHelper() {
    return _databaseHelper;
  }

  Future<Database> get database async {
    _database ??= await initDb();

    return _database!;
  }

  Future<Database> initDb() async {
    Directory directory = await getApplicationDocumentsDirectory();
    String path = '${directory.path}payments.db';

    var db = await openDatabase(path, version: 1, onCreate: _createDb);
    return db;
  }

  void _createDb(Database db, int newVersion) async {
    await db.execute(
        '''CREATE TABLE $tableName($colId INTEGER PRIMARY KEY AUTOINCREMENT, $colFirstMounth REAL, 
        $colLastMounth REAL, $colFullPayments REAL, $colSumm INTEGER, $colPercents REAL, 
        $colDate INTEGER, $colDateType TEXT, $colPaymentType TEXT)''');
  }

  Future<int> clearDb() async {
    Database db = await database;
    int result = await db.delete(tableName);

    return result;
  }
}
