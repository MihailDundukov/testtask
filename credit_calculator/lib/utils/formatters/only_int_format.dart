import 'package:credit_calculator/ui/widgets/custom_error_widget.dart';
import 'package:credit_calculator/values/colors.dart';
import 'package:credit_calculator/values/strings.dart';
import 'package:flutter/services.dart';
import 'package:get/get.dart';

class OnlyIntFormatter extends TextInputFormatter {
  @override
  TextEditingValue formatEditUpdate(
    TextEditingValue oldValue,
    TextEditingValue newValue,
  ) {
    if (RegExp('[^0-9]').hasMatch(newValue.text)) {
      Get.showSnackbar(
        const GetSnackBar(
          backgroundColor: AppColors.error,
          messageText: CustomErrorWidget(message: AppStrings.inputOnlyCorrect),
        ),
      );

      return oldValue;
    }

    SnackbarController.cancelAllSnackbars();
    return newValue;
  }
}
