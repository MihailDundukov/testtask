import 'package:credit_calculator/ui/widgets/custom_error_widget.dart';
import 'package:credit_calculator/values/colors.dart';
import 'package:credit_calculator/values/strings.dart';
import 'package:flutter/services.dart';
import 'package:get/get.dart';

class OnlyNumFormatter extends TextInputFormatter {
  @override
  TextEditingValue formatEditUpdate(
    TextEditingValue oldValue,
    TextEditingValue newValue,
  ) {
    const snackbar = GetSnackBar(
      backgroundColor: AppColors.error,
      messageText: CustomErrorWidget(message: AppStrings.inputOnlyCorrect),
    );

    int dotCnt = 0;
    for (int i = 0; i < newValue.text.length; i++) {
      if (newValue.text[i] == '.') {
        dotCnt++;
      }
    }

    if ((newValue.text.length == 1 &&
            RegExp('[^0-9]').hasMatch(newValue.text)) ||
        dotCnt > 1) {
      Get.showSnackbar(snackbar);

      return oldValue;
    } else if (newValue.text.length > 1 &&
        RegExp('[^0-9.]').hasMatch(newValue.text)) {
      Get.showSnackbar(snackbar);

      return oldValue;
    }

    SnackbarController.cancelAllSnackbars();
    return newValue;
  }
}
