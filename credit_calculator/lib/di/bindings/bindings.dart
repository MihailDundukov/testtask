import 'package:credit_calculator/getx_controllers/calculator_controller.dart';
import 'package:credit_calculator/getx_controllers/history_controller.dart';
import 'package:get/get.dart';

class CalculatorBindings implements Bindings {
  @override
  void dependencies() {
    Get.put(CalculatorController());
    Get.put(HistoryController());
  }
}
